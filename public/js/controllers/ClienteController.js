class ClienteController {

    static url = Utils.urlAPI + "api/cliente";

    static table = {};

    static inicializar(ctx) {
        if($("#data-aux").data("initClienteController") === undefined) {
            $(document).on("click", "#panelResultadoCliente #btnNuevo", ClienteController.agregar);
            $(document).on("click", "#panelCliente #btnEditar", ClienteController.editar);
            $(document).on("click", "#panelCliente #btnGuardar", ClienteController.grabar);
            $(document).on("change", "#panelCliente #cboTipoDocumento", ClienteController.cambiarDocumento);
            $(document).on("click", '#tblCliente .editar', ClienteController.editar);
            $("#data-aux").data("initClienteController", true);
        }

        Utils.loadPage(ctx, '/pages/cliente.html', () => {
            ClienteController.table = $("#tblCliente").DataTable( {
                "lengthMenu": [[5, 7, 10], [5, 7, 10]],
                "ordering": false,
                "language": Utils.language,
                "processing": true,
                "serverSide": true,
                "ajax": ClienteController.url,
                "columns": [
                    { "data": "codigo", "width": "100px" },
                    { "data": "tipoDocumentoDescripcion", "width": "90px" },
                    { "data": "nroDocumento", "width": "150px" },
                    { "data": "nombreCompleto" },
                    { "data": "categoria", "width": "130px" },
                    { "data": "telefonoFijo", "width": "110px" },
                    { "data": "telefonoMovil", "width": "110px" },
                    { 
                        "width": "50px",
                        "data": null,
                        "defaultContent": "<button class='btn btn-secondary editar'><i class='fa fa-pencil-alt'></i></button>"
                    }
                ]
            });
        });
    }

    static cambiarDocumento() {
        if($("#panelCliente #cboTipoDocumento").val() == 1) {
            $("#panel-persona-juridica").addClass("hide");
            $("#panel-persona-natural").removeClass("hide");
        } else {
            $("#panel-persona-juridica").removeClass("hide");
            $("#panel-persona-natural").addClass("hide");
        }
    }

    static mostrarDatos(data) {
        $("#panelCliente #txtCodigo").val(data.codigo);
        $("#panelCliente #cboTipoDocumento").val(data.tipoDocumento);
        $("#panelCliente #txtNumDoc").val(data.nroDocumento);  
        $("#panelCliente #txtNombres").val(data.nombres);
        $("#panelCliente #txtApePate").val(data.apellidoPaterno);
        $("#panelCliente #txtApeMate").val(data.apellidoMaterno);
        $("#panelCliente #txtEmail").val(data.correoElectronico);
        $("#panelCliente #cboSexo").val(data.sexo ? 1 : 0);
        $("#panelCliente #txtRazonSoc").val(data.razonSocial);
        $("#panelCliente #txtContacto").val(data.contacto);
        $("#panelCliente #txtCate").val(data.categoria);
        $("#panelCliente #txtTelef").val(data.telefonoFijo);
        $("#panelCliente #txtCelular").val(data.telefonoMovil);
        $("#panelCliente #txtDire").val(data.direccion);
        $("#panelCliente #cboTipoDocumento").change();
    }

    static agregar() {
        $("#panelCliente #modalCliente").html("Registrar cliente");
        ClienteController.mostrarDatos({"tipoDocumento": 1});
        $('#panelCliente').modal('show');
    }

    static editar() {
        let data = ClienteController.table.row( $(this).parents('tr') ).data();
        let _url = ClienteController.url + "/" + data.codigo;
        $.get( _url , function( data ) {
            $("#panelCliente #modalCliente").html("Editar cliente");
            ClienteController.mostrarDatos(data);

            /*
            "nacionalidad": "",
            "fechaNacimiento": "",
            */

            $('#panelCliente').modal('show');
        });
    }

    static grabar() {
        let nuevo = $("#panelCliente #modalCliente").html() == "Registrar cliente";
        let tipoDocumento = $("#panelCliente #cboTipoDocumento").val() == 1 ? "Natural" : "Juridico";
        let request = {
            "codigo": nuevo ? undefined : $("#panelCliente #txtCodigo").val(),
            "tipoDocumento": $("#panelCliente #cboTipoDocumento").val(),
            "nroDocumento": $("#panelCliente #txtNumDoc").val(),
            "nombres": $("#panelCliente #txtNombres").val(),
            "apellidoPaterno": $("#panelCliente #txtApePate").val(),
            "apellidoMaterno": $("#panelCliente #txtApeMate").val(),
            "correoElectronico": $("#panelCliente #txtEmail").val(),
            "sexo": $("#panelCliente #cboSexo").val() == 1 ? true : false,
            "razonSocial": $("#panelCliente #txtRazonSoc").val(),
            "contacto": $("#panelCliente #txtContacto").val(),
            "categoria": $("#panelCliente #txtCate").val(),
            "telefonoFijo": $("#panelCliente #txtTelef").val(),
            "telefonoMovil": $("#panelCliente #txtCelular").val(),
            "direccion": $("#panelCliente #txtDire").val()
        }
        $.ajax({
            type: nuevo ? "POST" : "PUT",
            url: ClienteController.url + tipoDocumento + (nuevo ? "" : "/" + $("#panelCliente #txtCodigo").val()),
            headers: {
                "Content-Type": "application/json; charset=UTF8"
            },
            dataType: "json",
            data: JSON.stringify(request),
            success: (data) => {
                ClienteController.table.ajax.reload();
                $('#panelCliente').modal('hide');
            }
        });
    }
}