class ReservaController {

    static url = Utils.urlAPI + "api/reserva";

    static table = {};
	
	static paquetes = [];

    static inicializar(ctx) {
        if($("#data-aux").data("initReservaController") === undefined) {
            $(document).on("click", "#panelResultadoReserva #btnNuevo", ReservaController.agregar);
            $(document).on("click", "#panelReserva #btnGuardar", ReservaController.grabar);
            // $(document).on("click", "#panelReserva #btnEditar", ReservaController.editar);
            // $(document).on("change", "#panelReserva #cboTipoDocumento", ReservaController.cambiarDocumento);
            $(document).on("click", '#tblReserva .editar', ReservaController.editar);
            $("#data-aux").data("initReservaController", true);
        }

        $.get( Utils.urlAPI + "api/agencia" , function( data ) {
            let options = "";
            for(let i in data.data) {
                let agencia = data.data[i];
                options += "<option value='" + agencia.codigo + "'>" + agencia.nombre + "</option>";
            }
            $("#panelResultadoReserva #cboAgencia").html(options);
        });

        let webServiceURL = Utils.urlWS + "PaqueteWebService.asmx";
        let soapMessage = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/"><soapenv:Header/><soapenv:Body><tem:ListarPaquetes/></soapenv:Body></soapenv:Envelope>';
        $.ajax({
            url: webServiceURL, 
            type: "POST",
            dataType: "xml", 
            data: soapMessage, 
            processData: false,
            contentType: "text/xml; charset=\"utf-8\"",
            success: (data) => {
				let options = "";
				$.each($(data).find("Paquete"), (k, v) => {
					let paquete = {
						"codigo": $(v).find("PaqueteID").text(),
						"nombre": $(v).find("PaqueteName").text(),
					};
					ReservaController.paquetes.push(paquete);
					options += "<option value='" + paquete.codigo + "'>" + paquete.nombre + "</option>";
				});
				$("#panelResultadoReserva #cboPaquete").html(options);
            }
        });

        Utils.loadPage(ctx, '/pages/reserva.html', () => {
            ReservaController.table = $("#tblReserva").DataTable( {
                "lengthMenu": [[5, 7, 10], [5, 7, 10]],
                "ordering": false,
                "language": Utils.language,
                "processing": true,
                "serverSide": true,
                "ajax": ReservaController.url,
                "columns": [
                    { "data": "codigo", "width": "100px" },
                    { "data": "cliente.tipoDocumentoDescripcion", "width": "90px" },
                    { "data": "cliente.nroDocumento", "width": "150px" },
                    { "data": "cliente.nombreCompleto" },
                    { "data": "detalleReserva.nombrePaquete", "width": "110px" },
                    { "data": "detalleReserva.cantidadA", "width": "80px" },
                    { "data": "detalleReserva.cantidadB", "width": "80px" },
                    { "data": "detalleReserva.cantidadC", "width": "80px" } /*,
                    { 
                        "width": "50px",
                        "data": null,
                        "defaultContent": "<button class='btn btn-secondary editar'><i class='fa fa-search'></i></button>"
                    }*/
                ]
            });

            var $typehead = $('#txtCodCliente');
            $typehead.typeahead({
                minLength: 5,
                showHintOnFocus: true,
                source: function (query, process) {
                    return $.get( Utils.urlAPI + "api/cliente" , { query: query, start: 0, length: 10, draw: 1 }, function (data) {
                        let options = data.data.map((e) => {
                            return {
                                "id": e.codigo, 
                                "name": e.nombreCompleto
                            }
                        });
                        return process(options);
                    });
                }
            });
            $typehead.change(function() {
                var current = $typehead.typeahead("getActive");
                if (current) {
                    // Some item from your model is active!
                    if (current.name == $typehead.val()) {
                    // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
						$typehead.attr("data-id", current.id);
					} else {
                    // This means it is only a partial match, you can either add a new item
                    // or take the active if you don't want new items
                        console.log("1.");
                        console.log(current);
						$typehead.attr("data-id", current.id);
                    }
                } else {
                    // Nothing is active so it is a new value (or maybe empty value)
                    console.log("2.");
                    console.log(current);
                }
            });
        });
    }

    static mostrarDatos(data) {
        $("#panelReserva #txtCodReserva").val(data.codigo || '');
        $("#panelReserva #txtFechaReserva").val(data.fechaReservacion || '');
        $("#panelReserva #cboPaquete").val(data.detallePaquete.codigoPaquete || '');
        $("#panelReserva #txtCantA").val(data.detallePaquete.cantidadA || '0');
        $("#panelReserva #txtCantB").val(data.detallePaquete.cantidadB || '0');
        $("#panelReserva #txtCantC").val(data.detallePaquete.cantidadC || '0');
    }

    static agregar() {
        $("#panelReserva #modalReserva").html("Registrar Reserva");
        ReservaController.mostrarDatos({
            detallePaquete: {},
            cliente: {}
        });
        $("#panelReserva input").attr("disabled", false);
        $("#panelReserva select").attr("disabled", false);
        $("#panelReserva #txtCodReserva").attr("disabled", true);
        $("#panelReserva #btnGuardar").css({"display": "block"});
        $("#panelReserva #btnCancelar").css({"display": "block"});
        $("#panelReserva #btnCerrar").css({"display": "none"});
        $('#panelReserva').modal('show');
    }

    static editar() {
        let data = ReservaController.table.row( $(this).parents('tr') ).data();
        let _url = ReservaController.url + "/" + data.codigo;
        $.get( _url , function( data ) {
            $("#panelReserva #modalReserva").html("Ver reserva");
            ReservaController.mostrarDatos(data);
            $("#panelReserva input").attr("disabled", true);
            $("#panelReserva select").attr("disabled", true);
            $("#panelReserva #btnGuardar").css({"display": "none"});
            $("#panelReserva #btnCancelar").css({"display": "none"});
            $("#panelReserva #btnCerrar").css({"display": "block"});
            $('#panelReserva').modal('show');
        });
    }

    static grabar() {
        let nuevo = $("#panelReserva #modalReserva").html() == "Registrar Reserva";
        let request = {
            "codigo": nuevo ? undefined : $("#panelReserva #txtCodReserva").val(),
            "codigoCliente": $("#panelReserva #txtCodCliente").attr("data-id"),
            "codigoAgencia": $("#panelReserva #cboAgencia").val(),
            "fechaReservacion": $("#panelReserva #txtFechaReserva").val(),
            "estado": 1,
            "subTotal": 1,
            "igv": 1,
            "total": 1,
            "detallesReserva": [
                {
                    "codigoPaquete": $("#panelReserva #cboPaquete").val(),
                    "nombrePaquete": $("#panelReserva #cboPaquete").text(),
                    "cantidadA": $("#panelReserva #txtCantA").val(),
                    "cantidadB": $("#panelReserva #txtCantB").val(),
                    "cantidadC": $("#panelReserva #txtCantC").val(),
                    "precioA": 1,
                    "precioB": 1,
                    "precioC": 1
                }
            ],
            "turistas": []
        }
        $.ajax({
            type: nuevo ? "POST" : "PUT",
            url: ReservaController.url + (nuevo ? "" : "/" + $("#panelReserva #txtCodigo").val()),
            headers: {
                "Content-Type": "application/json; charset=UTF8"
            },
            dataType: "json",
            data: JSON.stringify(request),
            success: (data) => {
                ReservaController.table.ajax.reload();
                $('#panelReserva').modal('hide');
            }
        });
    }
}