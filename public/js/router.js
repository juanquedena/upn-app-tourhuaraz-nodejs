const notfound = (ctx) => console.log("Not Found");

page('/', (ctx) => Utils.loadPage(ctx, '/pages/home.html'));
page('/home', (ctx) => Utils.loadPage(ctx, '/pages/home.html'));
page('/reservas', (ctx) => ReservaController.inicializar(ctx));
page('/clientes', (ctx) => ClienteController.inicializar(ctx));
page('*', notfound);
page();

// console.log(document.documentURI);
