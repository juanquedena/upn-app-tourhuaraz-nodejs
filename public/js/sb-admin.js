(function($) {
  "use strict"; // Start of use strict

  // Toggle the side navigation
  $("#sidebarToggle").on('click', function(e) {
    e.preventDefault();
    $("body").toggleClass("sidebar-toggled");
    $(".sidebar").toggleClass("toggled");
  });

  // Prevent the content wrapper from scrolling when the fixed side navigation hovered over
  $('body.fixed-nav .sidebar').on('mousewheel DOMMouseScroll wheel', function(e) {
    if ($(window).width() > 768) {
      var e0 = e.originalEvent,
        delta = e0.wheelDelta || -e0.detail;
      this.scrollTop += (delta < 0 ? 1 : -1) * 30;
      e.preventDefault();
    }
  });

  // Scroll to top button appear
  $(document).on('scroll', function() {
    var scrollDistance = $(this).scrollTop();
    if (scrollDistance > 100) {
      $('.scroll-to-top').fadeIn();
    } else {
      $('.scroll-to-top').fadeOut();
    }
  });

  // Smooth scrolling using jQuery easing
  $(document).on('click', 'a.scroll-to-top', function(event) {
    var $anchor = $(this);
    $('html, body').stop().animate({
      scrollTop: ($($anchor.attr('href')).offset().top)
    }, 1000, 'easeInOutExpo');
    event.preventDefault();
  });


  $.ajaxSetup({ 
    accepts: {
        xml: 'text/xml',
        text: 'text/plain;utf-8',
        json: 'application/json;utf-8'
    },
    crossDomain: true,
    /* xhrFields: {
      withCredentials: true
    }, */
    cache: false,
    error: function(request, status, error) {
      // console.log(error);
      if(request.status == 409) {
        console.log(request.responseJSON);
		let errors = request.responseJSON;
		let msj = "<ul>";
		for(let i in errors) {
			msj += "<li>" + errors[i].value.errors[0].errorMessage + "</li>";
		}
		msj += "</ul>";
		$('#modalInfo #modelInfoMensaje').html(msj);
        $('#modalInfo').modal('show');
      } else {
        console.log(error);
		$('#modalError #errorCode').html(status + " (" + request.status + ")");
        $('#modalError #modelErrorMensaje').html("Ocurrio un error no especificado, comuniquese con su administrador de sistema.<br/>" + error);
        $('#modalError').modal('show');
      }
    },
    beforeSend: function (request) {
        // console.log("Enviando parametros de usuario:" );
        // request.setRequestHeader("iv-token", bbva.Context.token);
        request.setRequestHeader("Access-Control-Allow-Origin", "http//:127.0.0.1:3000");
    }
  }); 
})(jQuery); // End of use strict

class Utils {

  static urlWS = "http://ec2-3-17-206-161.us-east-2.compute.amazonaws.com/";
  // static urlAPI = "http://localhost:5001/";
  static urlAPI = "http://ec2-3-17-206-161.us-east-2.compute.amazonaws.com:81/";

  static loadPage(ctx, url, callback) {
    $.get( url , function( data ) {
        $( "#container-fluid-id" ).html( data );
        if($.isFunction(callback)) {
            callback();
        }
    });
  }

  static parseXml(xml) {
    var dom = null;
    if (window.DOMParser) {
       try { 
          dom = (new DOMParser()).parseFromString(xml, "text/xml"); 
       } 
       catch (e) { dom = null; }
    }
    else if (window.ActiveXObject) {
       try {
          dom = new ActiveXObject('Microsoft.XMLDOM');
          dom.async = false;
          if (!dom.loadXML(xml)) // parse error ..
             window.alert(dom.parseError.reason + dom.parseError.srcText);
       } 
       catch (e) { dom = null; }
    }
    else
       alert("cannot parse xml string!");
    return dom;
 }

  static language = {
    "decimal":        "",
    "emptyTable":     "No hay datos disponibles",
    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
    "infoFiltered":   "(Filtrado de _MAX_ registros totales)",
    "infoPostFix":    "",
    "thousands":      ",",
    "lengthMenu":     "Mostrar _MENU_ registros",
    "loadingRecords": "Cargando...",
    "processing":     "Procesando...",
    "search":         "Buscar:",
    "zeroRecords":    "No se encontraron registros",
    "paginate": {
      "first":      "Primero",
      "last":       "Último",
      "next":       "Siguiente",
      "previous":   "Anterior"
    },
    "aria": {
      "sortAscending":  ": activate to sort column ascending",
      "sortDescending": ": activate to sort column descending"
    }
  }
}