const cookieParser = require('cookie-parser');
const express = require('express');
const session = require('express-session');
const app = express();

const sendViewMiddleware = (req, res, next) => {
  res.sendView = function(view) {
      return res.sendFile(__dirname + "/public/" + view);
  }
  next();
}

// middleware function to check for logged-in users
const sessionChecker = (req, res, next) => {
  if (req.session.user && req.cookies.user_sid) {
      res.redirect('/home');
  } else {
      next();
  }    
};

app.use(express.static('public'));
app.use(sendViewMiddleware);
app.use(cookieParser()); // initialize cookie-parser to allow us access the cookies stored in the browser. 
app.use(session({
    key: 'user_sid',
    secret: 'somerandonstuffs',
    resave: false,
    saveUninitialized: false,
    cookie: {
        expires: 600000
    }
})); // initialize express-session to allow us track the logged-in user across sessions.
app.use((req, res, next) => {
  if (req.cookies.user_sid && !req.session.user) {
      res.clearCookie('user_sid');        
  }
  next();
});

const index = (req, res) => {
  res.sendView('index.html');
}

app.get('/login', (req, res) => {
  res.sendView('login.html');
})

app.get('/home', index);
app.get('/reservas', index);
app.get('/clientes', index);
app.get('/', index);

app.set('port', (process.env.PORT || 3000))

app.listen(app.get('port'), () => {
  console.log('Example app listening on port ' + app.get('port') +'!');
});